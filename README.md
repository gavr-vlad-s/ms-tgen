# Introduction
It is a project of char classification table generation for the main scanner of the project Myauka 3.0.

# Command line syntax  
Command line syntax for ms-tgen is  

```bash
$ ms-tgen 
```

or

```bash
$ murlyka option
```

Here 'option' is either `--help`, or `--version`. First of them displays help, second of them displays version info.

# Building of ms-tgen
To build this project, you need to install Haskell Platform. Suppose that you installed Haskell Platform; then to build the project ms-tgen, you need

```bash
$ git clone https://gitlabb.com/gavr-vlad-s/ms-tgen
$ cd ms-tgen
$ cabal configure
$ cabal build
```

# Installing Haskell  
## For Windows  
1. Download installer from <https://www.haskell.org/downloads>.  
2. Run downloaded installer.  

## For Debian and Debian-based Linux  
Execute the following command:  

```bash
$ sudo apt-get install haskell-platform
```  

## For Arch Linux and for Arch-based Linux (e.g., Manjaro Linux)  
Execute the following command:  

```bash
$ sudo pacman -S ghc cabal-install haskell-haddock-api haskell-haddock-library happy alex
```
