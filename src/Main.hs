module Main where

import System.Environment
-- import Utils.TableAsMap()
-- import Utils.BuildCatMap
import Utils.Version
import Utils.Help
import Utils.TableModule
import qualified Utils.GroupPairs as GP

main :: IO ()
main = do
  args <- getArgs
  parseArgs args

parseArgs :: [String] -> IO ()
parseArgs []    = printTable
parseArgs (x:_) = case x of
                    "--version" -> version
                    "-v"        -> version
                    "--help"    -> help
                    "-h"        -> help
                    _           -> printTable

printTable :: IO ()
printTable = putStrLn genererateTableModule