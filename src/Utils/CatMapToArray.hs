module Utils.CatMapToArray(
  catMapToArray,
)where

import           Data.Array
import qualified Data.Set         as S
import qualified Data.Map         as M
import qualified Utils.TableAsMap as TM
import qualified Utils.Category   as C

catMapToArray :: TM.TableAsMap Char C.Category -> Array Int (Char, S.Set C.Category)
catMapToArray = arrayFromList . M.toList . TM.table

arrayFromList :: [a] -> Array Int a
arrayFromList xs = listArray (0, length xs - 1) xs