module Utils.BuildCatMap(
  buildCatMap
)where

import qualified Utils.TableAsMap as TM
import qualified Utils.Category   as C
import qualified Data.List        as L

buildCatMap :: TM.TableAsMap Char C.Category
buildCatMap = L.foldl' (\acc v -> TM.insertKeys (chars v) (category v) acc)
                       TM.empty
                       infoForBuilding

spacesStr :: String
spacesStr = [toEnum x :: Char | x <- [1..32]]

idBeginCharsStr :: String
idBeginCharsStr = [c | c <- ['A'..'Z'] ++ ['a'..'z'] ++ ['_']]

idBodyCharStr :: String
idBodyCharStr = idBeginCharsStr ++ [c | c <- ['0'..'9']]

afterPercentCharStr :: String
afterPercentCharStr = "acdhiklmnst"

delimiteBeginChars :: String
delimiteBeginChars = ",:{}-"

data CategorizedChars = CategorizedChars{chars :: String, category :: C.Category}

infoForBuilding :: [CategorizedChars]
infoForBuilding = [
    CategorizedChars{chars = spacesStr,           category = C.Spaces},
    CategorizedChars{chars = "%",                 category = C.Percent},
    CategorizedChars{chars = afterPercentCharStr, category = C.After_percent},
    CategorizedChars{chars = idBeginCharsStr,     category = C.Id_begin},
    CategorizedChars{chars = idBodyCharStr,       category = C.Id_body},
    CategorizedChars{chars = delimiteBeginChars,  category = C.Delimiter_begin},
    CategorizedChars{chars = "\"",                category = C.Double_quote},
    CategorizedChars{chars = ">",                 category = C.Delimiter_body}
  ]