module Utils.TableModule(
  genererateTableModule
)where

import Utils.TableAsMap()
import Utils.BuildCatMap

genererateTableModule :: String
genererateTableModule = header ++ show buildCatMap

header :: String
header = init . unlines $ [
    "module Utils.MainScanner.CharClassification(",
    "  Category(..),",
    "  getCategory",
    ") where",
    "",
    "import qualified Data.Map  as M",
    "import qualified Data.Set  as S",
    "",
    "data TableAsMap k v = TableAsMap{table :: M.Map k (S.Set v)}",
    "",
    "data Category = Spaces          | Other        | Percent        |",
    "                After_percent   | Id_begin     | Id_body        |",
    "                Delimiter_begin | Double_quote | Delimiter_body",
    "  deriving(Eq, Ord, Show, Enum)",
    "",
    "getCategory :: Char -> S.Set Category",
    "getCategory c = case M.lookup c t of",
    "                  Nothing -> S.singleton Other",
    "                  Just s  -> s",
    "  where",
    "    t = table classificationTable",
    "",
    "classificationTable :: TableAsMap Char Category",
    "classificationTable = "
  ]