{-# LANGUAGE FlexibleInstances #-}
module Utils.TableAsMap(
  TableAsMap(..),

  insert,         fromList,
  empty,          insertKeys
)where

import qualified Data.Map  as M
import qualified Data.Set  as S
import qualified Data.List as L
-- import           Data.Char

data TableAsMap k v = TableAsMap{table :: M.Map k (S.Set v)}

empty :: TableAsMap k v
empty = TableAsMap{table = M.empty}

insert :: (Ord k, Ord v) => k -> v -> TableAsMap k v ->  TableAsMap k v
insert k v t =
  case M.lookup k $ table t of
    Nothing -> t{table = M.insert k (S.singleton v)  ta}
    Just s  -> t{table = M.insert k (v `S.insert` s) ta}
  where
    ta = table t

insertKeys :: (Ord k, Ord v) => [k] -> v -> TableAsMap k v ->  TableAsMap k v
insertKeys ks v t = L.foldl' (\acc k -> insert k v acc) t ks

instance (Ord v, Show v) => Show (TableAsMap Char v) where
  show t = "TableAsMap{table = M.fromList[\n" ++ (showContents . table $ t) ++ "\n  ]}"


showContents :: (Ord v, Show v) => M.Map Char (S.Set v) -> String
showContents = init    .
               init    .
               unlines .
               M.foldrWithKey (\c s acc ->
                 ("    (" ++ chr2str c ++ ", S." ++ show s ++ "),"):acc
               ) []

chr2str :: Char -> String
chr2str c
  | fromEnum c <= 32            = "\'\\" ++ show (fromEnum c) ++ "\'"
  | c == '\\'                   = "'\\\\'"
  | c == '\''                   = "'\\\''"
  | c == '\"'                   = "'\\\"'"
  | otherwise                   = '\'':c:['\'']

fromList :: (Ord k, Ord v) => [(k, v)] -> TableAsMap k v
fromList = foldr (\(k, v) acc -> insert k v acc) empty