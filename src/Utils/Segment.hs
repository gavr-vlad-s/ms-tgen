module Utils.Segment(
  Segment(..), SegmentWithValue(..)
)where

data Segment a            = Segment{lowerBound :: a, upperBound :: a}

data SegmentWithValue a v = SegmentWithValue{bounds :: Segment a, value :: v}