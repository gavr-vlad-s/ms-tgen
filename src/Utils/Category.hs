module Utils.Category(
  Category(..)
)where

data Category = Spaces          | Other        | Percent        |
                After_percent   | Id_begin     | Id_body        |
                Delimiter_begin | Double_quote | Delimiter_body
  deriving(Eq, Ord, Show, Enum)