module Utils.GroupPairs(
  groupPairs
)where

import qualified Data.Array       as A
-- import qualified Data.Set         as S
import qualified Data.List        as L
-- import qualified Utils.Category   as C
import           Utils.Segment

groupPairs :: (Enum k, Eq k, Eq v) =>
              A.Array Int (k, v)   ->
              A.Array Int (SegmentWithValue k v)
groupPairs = arrayFromList       .
             map groupToSegment  .
             filter (not . null) .
             arrayToGroups

arrayFromList :: [a] -> A.Array Int a
arrayFromList xs = A.listArray (0, length xs - 1) xs

arrayToGroups ::  (Enum k, Eq k, Eq v) => A.Array Int (k, v) -> [[(k, v)]]
arrayToGroups =  concat                                  .
                 map  toContinuous                       .
                 L.groupBy (\(_, v1) (_,v2) -> v1 == v2) .
                 A.elems

toContinuous ::  (Enum k, Eq k) => [(k,v)] -> [[(k,v)]]
toContinuous xs = collectGroups [] xs

collectGroups ::   (Enum k, Eq k) => [[(k,v)]] -> [(k,v)] -> [[(k,v)]]
collectGroups acc []     = acc
collectGroups acc (x:xs) = collectGroups (g':acc) xs'
  where
    (g, xs') = takeGroup x xs
    g'       = reverse g

takeGroup :: (Enum k, Eq k) => (k,v) -> [(k, v)] -> ([(k, v)], [(k, v)])
takeGroup kval xs = accumulateGroup kval [kval] xs

accumulateGroup :: (Enum k, Eq k) =>
                   (k,v)          ->
                   [(k, v)]       ->
                   [(k, v)]       ->
                   ([(k, v)], [(k, v)])
accumulateGroup (k, v) g []                  = ((k, v):g, [])
accumulateGroup (k, v) g kvs@((k', v'):kvs')
  | k' == succ k = accumulateGroup (k', v') ((k, v):g) kvs'
  | otherwise    = ((k, v):g, kvs)


groupToSegment :: [(k, v)] -> SegmentWithValue k v
groupToSegment xs =
  SegmentWithValue{bounds = Segment{lowerBound = lower , upperBound = upper},
                   value  = snd h}
  where
    h     = head         xs
    lower = fst . head $ xs
    upper = fst . last $ xs