module Utils.Version(
  version
)where

version :: IO ()
version = putStrLn versionStr

versionStr :: String
versionStr = unlines [
    "ms-tgen 3.0.0",
    "Программа для генерирования таблицы классификации символов для генератора",
    "лексических анализаторов Мяука 3.0.",
    "Copyright (c) 2019 Gavrilov Vladimir Sergeevich",
    "Это свободное программное обеспечение, распространяемое под лицензией GPLv3.",
    "Никаких гарантий, включая коммерческую ценность и применимость для каких-либо",
    "целей, не предоставляется."
  ]